# Introduction 

Ce plugin permet d'automatiser les [réponses aux DT/DICT](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23491) en générant plans et récépissés. Il est composé de deux modules principaux, _configuration_ et _traitement._

Grand merci aux [contributeurs et financeurs du projet](https://oslandia.gitlab.io/qgis/DICT/development/sponsors.html). 

# <img src="../_static/images/config.png"  width="32" height="32"> Configuration

La fenêtre configuration du plugin présente quatre onglets. 

## Informations concernant l'exploitant

Il s'agit ici des informations de l'exploitant utilisées ensuite par défaut. Les champs correspondent aux encarts suivants sur le CERFA de réponse : 

- *Coordonnées de l'exploitant*
- *Responsable du dossier*
- *Signature de l'exploitant ou de son représentant*

![config_informations](../_static/images/config_informations.png)

## Répertoires et formatage des fichiers

Configuration des chemins des différents répertoires et formatage des noms de fichiers. 

![config_sorties](../_static/images/config_sorties.png)

- **1** : répertoire de stockage des fichiers XML
- **2** : répertoire d'enregistrement des plans et récépissés. Le plugin enregistre chaque réponse **dans un répertoire différent**. C'est pourquoi il faut utiliser la variable `dict_no_teleservice` dans le chemin de sortie pour que le ZIP créé par le plugin ne contienne *que les fichiers concernant la réponse*.
- **3** : répertoire de stockage des mises en page de plans. 
  - *Lors de la première utilisation, les gabarits fournis avec le plugin sont copiés dans ce répertoire. Il est possible de les personnaliser sans perdre l'original.*
  - *Pour créer vos propres modèles il faut 
    - respecter le préfixe *DICT* pour les gabarits des mises en page
    - respecter le préfixe *TA_DICT* pour le gabarit du tableau d'assemblage*
- **4 et 5** : préfixes et suffixes utilisés pour le nommage des fichiers plans et récépissé; le nom est construit avec `<prefixe\>-<type_demande\>-<no_teleservice\>-<suffixe\>.` Avec la configuration de la copie d'écran, les noms pour une DT-DICT conjointe (N° 2021021206172D) seront :
  - pour la réponse :` Récépissé-DC-2021021206172D.pdf`
  - pour le plan : `PLAN-DC-2021021206172D.pdf`
    
*Il y aura un tiret suivi d'un numéro de page si il y a plusieurs folios, donc plusieurs fichiers PDF et un fichier préfixé de TA- pour le tableau d'assemblage.*

- **6** : répertoire de stockage des annexes (légende des plans, recommandations particulières à joindre aux récépissés, etc). Tous les fichiers présents dans ce répertoire seront systématiquement copiés dans le répertoire de la réponse
- **7** : échelles utilisables pour générer les plans PDF
- **8** : cocher la case pour générer systématiquement les plans PDF même lorsque le format DXF ou SHP est sélectionné   
- **9** : pour fusionner plusieurs PDF en un seul, il faut utiliser PDFTK. *En activant cette option, vos plans et le tableau d'assemblage seront automatiquement fusionnés. Le récépissé sera quant à lui seul.*
- **10** indiquer le chemin de l'exécutable ([à télécharger ici](http://www.angusj.com/pdftkb/)) sans l'interface graphique. À noter qu'il est recommandé d'utiliser Acrobat Reader pour la complétion du formulaire. Cette option est particulièrement recommandé pour les utilisateurs du plugin sur Unix dont les lecteurs PDF ne savent pas lire le format .xfdf permettant de remplir le Cerfa.

## Stockage des données en base

Cet onglet configure le stockage des informations des demandes/réponses **dans une base de données** afin de les suivre et les analyser.

Une fois la case _Stockage les demandes/réponses en base de données_ cochée, il faut d'abord choisir le moteur utilisé, ainsi que la connexion **existant déjà dans QGIS**.

- Les tables `en_cours` et `reponses` sont des **tables spatiales**.
- La colonne `geom` de la table `en_cours` contient l'**emprise** des travaux telle que définie dans le fichier .xml de la demande.
- La colonne `geom` de la table `reponses` contient une géométrie représentant l'**union des folios** créés pour générer les PDFs.

**Attention** les données ne sont enregistrées en base de données qu'après avoir cliqué sur le bouton *Enregistrer* de la boite de dialogue *Traitement de la demande*

### PostgreSQL

* Si les tables existent pas dans la base, il faut créer ou sélectionner le schéma destiné à  contenir ces tables, puis les créer et attacher les couches correspondant aux tables `en_cours` et `reponses`.
* Si les tables existent déjà dans la base, il faut sélectionner le schéma contenant ces tables, et attacher les couches correspondant aux tables `en_cours` et `reponses`. 

![config_database](../_static/images/config_database.png)

### Spatialite

Spatialite n'est pas supporté pour le moment.

### Compte de messagerie

Cet onglet permet d'envoyer le message en paramétrant le compte de messagerie (smtp) :

![config_mail.png](../_static/images/config_mail.png)

- **1** : le nom du serveur SMTP utiisé pour l'envoi des courriels (Orange, Gandi etc…), exemple : smtp.orange.fr
- **2** : adresse mail du compte SMTP envoyant le mail (expéditeur)
- **3** : mode de sécurité (Aucune, STARTTLS, SSL/TLS) --> utiliser de préférence SSL/TLS si supporté
- **4** : nom complet de l'expéditeur (champ FROM du courriel)
- **5** : port SMTP (par défaut 465 si SSL/TLS, 587 dans les autres cas)
- **6** : mot de passe du compte mail

Le bouton *Tester connexion* permet de vérifier la validité des paramètres saisis.

- **7** :  gabarit du courriel en texte brut
- **8** :  gabarit du courriel en HTML 
- **9** :  url de téléchargement du fichier ZIP contenant les éléments de la réponse (récépissé, plans, annexes..). Le contenu de ce champ est utilisé pour définir la variable `dict_url_reponse` apès résolution éventuelle des variables qu'il contient. Cette variable est utilisée dans le gabarit du courriel.

**NB** : pour que le lien de téléchargement fonctionne, il faut bien évidemment que le chemin du fichier défini dans l'url soit **accessible depuis l'internet**. Pour cela, soit le répertoire de sortie est directement accessible depuis internet, soit il est synchronisé avec un répertoire public grâce à **_rsync_** ou [syncthing](https://syncthing.net/) par exemple.

Au clic sur *OK*, un test de connexion est automatiquement effectué ; s'il est négatif, les paramètres ne sont pas sauvegardés. Le fichier ZIP ne sera créé et le courriel envoyé qu'au sur le bouton *Enregistrer* de la boite de dialogue *Traitement de la demande*. 

# <img src="../_static/images/icon.png"  width="32" height="32">  Traitement d'une demande

## Détail de l'interface

![dialog_dict](../_static/images/dialog_dict.png)

(figure A)

- **1** : sélection du fichier XML (le répertoire proposé est celui renseigné dans la configuration)
- **2** : choix de l'échelle (celles-ci sont prédéfinies conformément à la réglementation)
- **3** : taille de la mise en page (formats normalisés ISO)
- **4** : format de fichier des plans créés en cliquant sur le bouton (7)
- **5** : placement du ou des folios (insertion et orientation sur le plan d'autant de folios que nécessaire pour couvrir l'emprise)
- **6** : suppression du ou des folios
- **7** : génération du ou des plans en PDF, DXF ou SHP suivant le choix indiqué dans la liste déroulante *Format plans (4)*. 
  - Le format des plans proposé par défaut est fonction du choix du déclarant. Il est toujours possible de choisir un autre format pour fournir PDF et DXF par exemple.
  - Le DXF est créé dans le même répertoire que les autres éléments de réponse. Le(s) shape(s) sont généré(s) dans un sous-répertoire shp.
  - **NB** : toutes les couches visibles, sauf les couches `Emprise chantier` et `Folios` sont exportées en SHP.
- **8** : génération du PDF de réponse avec remplissage partiel des données à partir du fichier XML. Seules certaines parties sont à compléter par l'exploitant. Il est possible, que lors d'une demande de DT-DICT conjointe, le responsable du projet de travaux veuille être destinataire de la réponse (voir exemple ci-après)

![formulaire_sortie](../_static/images/formulaire_sortie.png)

Le système le signale alors par popup à l'utilisateur :

![dialog_messagebar](../_static/images/dialog_messagebar.png)

- 9 : purge des couches temporaires (*Emprise chantier* et *Folios*), suppression de toutes les informations de la boîte de dialogue et des variables associées

## Étapes de traitement

Pour le bon déroulement de la procédure l'ordre des opérations est le suivant (voir la copie d'écran ci-dessus) :

- **Étape 1. choix du XML** (1) ; la déclaration est alors analysée.
  - Les informations sur les travaux fournies  par le déclarant sont affichées sous le nom du fichier (type demande, adresse, commune principale, description...).
  - S'il a précisé la taille des plans, la mise en page correspondante est, si possible, sélectionnée.
  - S'il souhaite des fichiers vecteurs, cela est indiqué dns la boite de dialogue; de même s'il en a précisé le format (SHP ou DXF), celui-ci est sélectionné par défaut dans la liste déroulante *Format plans*
- **Étape 2. choix des échelles et du format** (2,3)
  - *Si les menus déroulants restent grisés, appuyez sur le bouton suppression de folios pour effectuer un rechargement. Si le problème persiste, rentrez à nouveau le chemin des modèles d'impression dans les paramètres*
- **Étape 3. placement des folios** (4)
  - Le 1er clic vous permet de choisir l'orientation du folio,
  - le 2ème valide l'orientation choisie (un clic droit, place le folio sans orientation),
  - le 3ème valide l'emplacement du folio.
  - À partir d'ici, chaque clic ajoute un folio (un clic droit permet toujours de revenir à un folio non orienté)
- **Étape 4. renouveler l'étape 3** autant que vous avez de folios à placer (il est important de garder pour l'ensemble de ces folios la même échelle et format)
- **Étape 5. génération des plans** (7)
- **Étape 6. génération du récépissé de réponse** (8)
  - *Dans votre réponse, il conviendra d'indiquer le nombre de pièces jointes. Ces pièces jointes regroupent les plans réalisés à partir des folios placés mais aussi un tableau d'assemblage qui indique au déclarant la manière dont sont positionnés ces folios. Ainsi, pour 1 seul plan aucun tableau d'assemblage ne sera nécessaire ; au-delà il vous faudra tenir compte de celui-ci dans les pièces jointes*
  - *Exemple : vous avez placé 2 folios, 1 tableau d'assemblage est nécessaire, donc il faudra indiquer 3 pièces jointes dans votre récépissé.*
  - *Dans le cas où vous feriez une fusion de l'ensemble des PDF, il n'est pas nécessaire d'indiquer de pièces-jointes, tous les documents seront présents dans un même PDF.*
- **Étape 7. clôturer la demande** en cliquant sur le bouton *Enregistrer*.
  * si l'enregistrement en base de données est configuré et activé, une nouvelle boite de dialogue s'ouvre pour saisir des dates de gestion. Saisir alors la date de réponse (proposée par défaut à la date du jour) et cliquer sur *Enregistrer* pour enregistrer les données en BD.
  * un fichier ZIP est alors créé, incluant le récépissé, les plans PDF et éventuellement les SHP ou DXF ainsi que les annexes; si configuré et activé, un courriel est envoyé au déclarant

![cloturer_demande](../_static/images/dialog_dict_cloturer.png)

- **Étape 8. [Facultatif] vider le plugin** (9) et recommencer le process autant que nécessaire

## Variables définies et utilisées par le plugin

Lors du traitement d'une déclaration, le plugin définit de manière dynamique, **au niveau du projet QGIS**, plusieurs variables qui seront utilisées dans les mises en pages, la préparation et l'envoi d'un courriel au délarant.

Les noms de ces variables sont tous préfixés par `dict` et contiennent des valeurs généralement issues du fichier XML :

![variables](../_static/images/variables.png)

| Variable | Valeur 
|-------------------|--------------
| `dict_filename` | nom du fichier PDF du récépissé (cerfa 14435)
| `dict_fullfilename` | nom complet du fichier XML
| `dict_no_teleservice` | numéro de la déclaration sur le guichet unique
| `dict_url_reponse` | url de téléchargement de la réponse (utilisée dans le corps du courriel)
| `dict_map_filename` | nom du fichier plan (sera éventuellement suffixé d'un tiret et de l'indice du plan s'il y a plusieurs folios)
| `dict_map_prefix` | préfixe du nom du fichier plan utilisé pour générer `dict_map_filename`
| `dict_map_suffix` | suffixe du nom du fichier plan utilisé pour générer `dict_map_filename`
| `dict_print_scale` | échelle des plans
| `dict_tvx_adresse` | adresse des travaux
| `dict_tvx_commune` | nom de la commune principale des travaux
| `dict_tvx_description` | description des travaux
| `dict_tvx_insee` | code INSEE de la commune principale des travaux
| `dict_type_demande` | type de la demande (DT, DICT, DC ou ATU)

*NB : lors de la purge, toutes les variables propres à la dernière déclaration sont effacées*.

----

```{toctree}
---
caption: Table des matières
maxdepth: 3
---
```

```{toctree}
---
caption: Références externes
maxdepth: 1
---
Le plugin sur le dépôt officiel de QGIS <https://plugins.qgis.org/plugins/DICT/>
```

