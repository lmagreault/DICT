"""
/***************************************************************************
 DICTAbout
                                 A QGIS plugin
 DICT
                             -------------------
        begin                : 2020-03-06
        git sha              : $Format:%H$
        copyright            : (C) 2020 by Loïc BARTOLETTI
        email                : loic.bartoletti@oslandia.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
import sys

from PyQt5 import uic
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QDialog

sys.path.append(os.path.dirname(__file__))

FORM_CLASS, _ = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "DICT_about.ui"), resource_suffix=""
)


class DICTAbout(QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(DICTAbout, self).__init__(parent)
        self.setupUi(self)

        self.rejected.connect(self.onReject)
        self.buttonBox.rejected.connect(self.onReject)
        self.buttonBox.accepted.connect(self.onAccept)

        self.init_logo(
            self.logo_oslandia, ":/plugins/DICT/resources/images/logo_oslandia.png"
        )
        self.init_logo(
            self.logo_megeve, ":/plugins/DICT/resources/images/logo_megeve.jpg"
        )
        self.init_logo(
            self.logo_azimut, ":/plugins/DICT/resources/images/logo_azimut.png"
        )
        self.init_logo(self.logo_sdev, ":/plugins/DICT/resources/images/logo_sdev.png")

    def init_logo(self, label, logo):
        img = QPixmap(logo)
        img = img.scaled(150, 150, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        label.setPixmap(img)

    def onAccept(self):
        self.accept()

    def onReject(self):
        self.close()
