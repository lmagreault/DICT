[general]
name=DICT
about=Le plugin permet de charger un XML d'une demande de DT/DICT pour exporter le plan à fournir avec le formulaire. Ce dernier est pré rempli par le plugin et se complète par un assistant reprennant le cerfa. Veuillez vous rendre sur la page du projet pour trouver la documentation.
category=Plugins
description=Assistant pour répondre aux DT/DICT (French only / Français seulement)
icon=icon.png
tags=DT,DICT,travaux,construire,france

# credits and contact
author=Loïc BARTOLETTI (Oslandia), Jean-Marie Arsac (Azimut)
email=qgis@oslandia.com
homepage=https://oslandia.gitlab.io/qgis/DICT/
tracker=https://gitlab.com/Oslandia/qgis/DICT/issues
repository=https://gitlab.com/Oslandia/qgis/DICT/

# experimental flag
deprecated=False
experimental=False
qgisMinimumVersion=3.10
qgisMaximumVersion=3.99

# versionnement
version=1.0.1
changelog=
 Version 1.0.1:
 - Amélioration de la documentation
 - Amélioration des performances pour la lecture du XML et la création des PDF
 - Correction du chemin pour l'envoi des mails
 - Correction de la lecture des gabarits
 - Correction de divers bugs dans l'interface
 Merci à nos contributeurs :
 Jean Claude Anotta - SDE des Vosges
 Quentin Jeanroy - Mairie de Megève
 Georgia Roesch - Colmar Agglo
 Laurent MAGRÉAULT - SIDEC du Jura
 Oslandia & Azimut

 Version 1.0.0:
 - Nouvelle version finalisée et accessible depuis le dépôt de QGIS.
 - Nouvelle interface graphique
 - Ajout de la gestion des folios
 - Ajout de l'export des plans en DXF/SHP
 - Ajout de la sauvegarde des DT-DICT dans une base PostgreSQL.
 - Ajout du support de l'envoi des mails
 - Amélioration de la documentation
 - Ajout de variables d'environnement
 L'interface de configuration reste la même mais est enrichie de deux onglets
 pour le stockage en base et l'envoi de courriel.
 La nouvelle documentation est disponible sur https://oslandia.gitlab.io/qgis/DICT/fr_guide/index.html

 Version 0.4.4-beta6:
 - Correction du code SQL pour les versions de PostgreSQL inférieure à 12
 - Amélioration du module mail
 - Amélioration de la documentation
 - Amélioration de l'UI


