**CONTEXTE**

Afin de prévenir les risques d'endommagement des réseaux enterrés, aériens ou
subaquatiques, les travaux projetés doivent être déclarés aux exploitants de ces
réseaux. Après avoir interrogé [le téléservice &quot; réseaux et canalisation &quot;](https://www.reseaux-et-canalisations.ineris.fr/gu-presentation/construire-sans-detruire/teleservice-reseaux-et-canalisations.html) qui recense les exploitants, le maître d'ouvrage et l'exécutant des travaux déclarent leur projet de travaux. Un [manuel](https://www.reseaux-et-canalisations.ineris.fr/gu-presentation/communication/manuels-dutilisation---declarants.html) sur le site du téléservice est à disposition des déclarants.

Ce plugin pour QGIS est destiné aux exploitants et automatise le travail nécessaire à la production des récépissés aux demandes des déclarations. Il prend en charge le format XML normalisé des demandes et permet de produire les formulaires CERFA réglementaires ainsi que les plans de situation et les annexes. Au préalable et pour l'utilisation du module, il est plus que nécessaire que l'exploitant soit destinataire des fichiers XML. Pour ce faire, lors de l'enregistrement sur le téléservice (Guichet Unique), il s'attachera à indiquer qu'il est en capacité à répondre sous format dématérialisé lorsque le déclarant lui fournira le fichier XML et PDF de la demande ([CERFA formulaire 14435\*04)](https://www.reseaux-et-canalisations.ineris.fr/gu-presentation/userfile?path=/CERFA_14435-04.pdf). Un [manuel](https://www.reseaux-et-canalisations.ineris.fr/gu-presentation/communication/manuels-dutilisation---exploitants.html) sur le site du téléservice est à disposition des exploitants.

Exemple d'un fichier XML où est indiqué que l'exploitant souhaite la version dématérialisée et les fichiers XML et PDF

# Introduction

Afin de répondre à certaines demandes en interne j'avais développé ce plugin
QGIS pour traiter les XML des DT-DICT.

Le développement a été abandonné durant plusieurs années, mais a repris grâce à l'intérêt porté par de nombreuses sollicitations et le travail fourni par Tristan Cazetti et Régis Bouquet.

Or, récemment une refonte a été entreprise par Jean-Claude ANOTTA du Syndicat Départemental d'Électricité des Vosges et Jean-Marie ARSAC de la société AZIMUT.

C'est à ce jour **la seule solution open source existante** sur le marché.

N'hésitez pas à nous contacter par [mail](mailto:infos@oslandia.com?subject=%5BDICT%5D%20Demande%20pour%20le%20plugin) si vous souhaitez faire évoluer ce plugin.

Vous trouverez plus d'informations sur notre [page dédiée](https://oslandia.com/en/offre-qgis/plugin-de-reponse-aux-dict).


🇫🇷 [Consulter la documentation](https://oslandia.gitlab.io/qgis/DICT/fr_guide/index.html)
