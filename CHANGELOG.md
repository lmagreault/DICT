# Changelog

## 1.0.1 - 2021/12/22

- Amélioration de la documentation
- Amélioration des performances pour la lecture du XML et la création des PDF
- Correction du chemin pour l'envoi des mails
- Correction de la lecture des gabarits
- Correction de divers bugs dans l'interface

Merci à nos contributeurs :
Jean Claude Anotta - SDE des Vosges
Quentin Jeanroy - Mairie de Megève
Georgia Roesch - Colmar Agglo
Laurent MAGRÉAULT - SIDEC du Jura

Oslandia & Azimut

## 1.0.0 - 2021/06/02

- Nouvelle version finalisée et accessible depuis le dépôt de QGIS.
- Nouvelle interface graphique
- Ajout de la gestion des folios
- Ajout de l'export des plans en DXF/SHP
- Ajout de la sauvegarde des DT-DICT dans une base PostgreSQL.
- Ajout du support de l'envoi des mails
- Amélioration de la documentation
- Ajout de variables d'environnement

L'interface de configuration reste la même mais est enrichie de deux onglets
pour le stockage en base et l'envoi de courriel.

La nouvelle documentation est disponible sur https://oslandia.gitlab.io/qgis/DICT/fr_guide/index.html

## 0.4.4-beta6 - 2021/05/05

- Correction du code SQL pour les versions de PostgreSQL inférieure à 12
- Amélioration du module mail
- Amélioration de la documentation
- Amélioration de l'UI

## 0.4.4-beta5 - 2021/04/29

- Corrections des exports DXF et SHP qui retournaient des fichiers vides
- Amélioration de l'UI du dialogue de traitement des DT/DICT
- Correction de la sauvegarde des cases à cocher pour la BDD et les mails
- Correction du script de création des données PostgreSQL

## 0.4.4-beta4 - 2021/04/26

- Ajout de l'export des plans en DXF/SHP
- Ajout de la sauvegarde des DICT dans une base PostgreSQL.

## 0.4.4-beta3 - 2021/04/09

- Corrections mineures

## 0.4.4-beta2 - 2020/12/15

- Nouvelle version publique

## 0.2.1 - 2020/08/26

- Ajout du support de la version 3.1 des fichiers XML

## 0.2.0 - 2020/03/12

- Portage du plugin pour QGIS 3
